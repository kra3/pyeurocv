#!/usr/bin/env python
# -*- coding:utf8 -*-

import os
from setuptools import setup, find_packages

ver = '0.1'
README = os.path.join(os.path.dirname(__file__), 'README')
long_desc = open(README).read() + '\n\n'

setup(name='pyeurocv',
      version=ver,
      author='Arun K.R.',
      author_email='the1.arun@gmail.com',
      url='https://bitbucket.org/kra3/pyeurocv',
      license='Simplified BSD',
      description='Python SDK for EuroCv.eu webservice',
      long_description=long_desc,
      keywords='eurocv',
      requires=['pysimplesoap'],
      install_requires=['setuptools', 'pysimplesoap'],
      packages=find_packages(),
      namespace_packages=['pyeurocv'],
      classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        ('Topic :: Software Development :: Libraries :: Python Modules'),
      ],
)
